package com.gothammanager.cashmanager.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.zxing.integration.android.IntentIntegrator
import com.gothammanager.cashmanager.R
import com.gothammanager.cashmanager.adapters.ProductsAdapter
import com.gothammanager.cashmanager.models.Supplier
import com.gothammanager.cashmanager.showToast
import kotlinx.android.synthetic.main.activity_products.*
import com.gothammanager.cashmanager.fragments.PaymentOptionsFragment

interface TotalValueInActivity {
    fun setTotalText(tot_value: Double = 0.0)
}

class ProductsActivity: AppCompatActivity(), TotalValueInActivity {
    private val adapter = ProductsAdapter(this, Supplier.products, this)
    private val fragmentManager = supportFragmentManager
    private var total: Double = 0.0
    companion object {
        var PAYMENT_FRAG: String = PaymentOptionsFragment::class.java.simpleName
    }
    /**
     * Create the NFCview and his components
     * @param savedInstanceState
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_products)
        setupRecyclerView()
        setTotalText()

        btn_scan_product.setOnClickListener {
            val scanner = IntentIntegrator(this)
            scanner.initiateScan()
        }

        btn_pay.setOnClickListener {
            val pay = PaymentOptionsFragment.newInstance(total_text_view.text.toString(), total)
            pay.show(fragmentManager, PAYMENT_FRAG)
        }
    }

    /**
     * Method to create the option of the menu
     * @param menu object menu
     */
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    /**
     * Method to define the action when button_settings is pressed
     * @param item icon which was pressed
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        return when (item.itemId) {
            R.id.btn_settings -> {
                val intent = Intent(this, LogInActivity::class.java)
                startActivity(intent)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    /**
     * Setup the view which will handle the list of the products
     */
    private fun setupRecyclerView() {
        val layoutManager = LinearLayoutManager(this)
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter

        btn_cancel_all.setOnClickListener {
            adapter.clearList()
        }
    }

    /**
     * Set the informative total text
     * @param tot_value Double with the total received by the back end
     */
    override fun setTotalText(tot_value: Double) {
        total = tot_value
        Log.e("settotaltext", tot_value.toString())
        total_text_view.text = "Total : ${String.format("%.02f", tot_value)}$"
        // total_text_view.text = "Total : " + String.format("%.02f", tot_value) + "$"
        if(tot_value == 0.0) {
            btn_pay.visibility = View.INVISIBLE
            adapter.clearListFront()
        } else {
            btn_pay.visibility = View.VISIBLE
        }
    }

    /**
     * Read the content from the codebar after the scan
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (result != null) {
            if (result.contents == null) {
                showToast("Could not read the bar-code", Toast.LENGTH_LONG)
            } else {
                adapter.addProduct(result.contents)
                Toast.makeText(this, "Scanned: " + result.contents, Toast.LENGTH_LONG).show()
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }
}