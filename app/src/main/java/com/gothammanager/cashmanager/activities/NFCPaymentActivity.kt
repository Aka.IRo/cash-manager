package com.gothammanager.cashmanager.activities

import android.app.PendingIntent
import android.content.Intent
import android.nfc.NdefMessage
import android.nfc.NfcAdapter
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.gothammanager.cashmanager.R
import com.gothammanager.cashmanager.models.Authentication
import com.gothammanager.cashmanager.models.Cart
import com.gothammanager.cashmanager.models.PaymentCard
import com.gothammanager.cashmanager.Constant.TOTAL_VALUE_KEY
import java.io.UnsupportedEncodingException
import kotlin.experimental.and

private val TAG: String = NFCPaymentActivity::class.java.simpleName
class NFCPaymentActivity : AppCompatActivity() {
    private var nfcAdapter: NfcAdapter? = null
    private lateinit var tvInfoPayment: TextView
    private lateinit var tvTotalValue: TextView
    private lateinit var mPendingIntent: PendingIntent
    /**
     * Create the NFCview and his components
     * @param savedInstanceState can contains the value of the total_text sent from ProductsActivity
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nfc_payment)
        tvInfoPayment = findViewById(R.id.tv_payment_instructions)
        tvTotalValue = findViewById(R.id.tv_total_value)

        val bundle: Bundle? = intent.extras
        bundle?.let{
            tvTotalValue.text = bundle.getString(TOTAL_VALUE_KEY)
        }
        nfcAdapter = NfcAdapter.getDefaultAdapter(this)
        if (nfcAdapter == null) {
            Toast.makeText(this, "NFC is not available", Toast.LENGTH_LONG).show()
            finish()
            return
        }
        mPendingIntent = PendingIntent.getActivity(this, 0, Intent(this, NFCPaymentActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0)
    }

    override fun onResume() {
        super.onResume()
        nfcAdapter?.enableForegroundDispatch(this, mPendingIntent, null, null)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        this.finish()
    }

    override fun onPause() {
        super.onPause()
        nfcAdapter?.disableForegroundDispatch(this)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        processIntent(intent)
    }

    /**
     * Method that will process the Intent read by the NFC. It will call the API and to the transaction
     * @param intent intent received from the NFCReader
     */
    private fun processIntent(intent: Intent?) {
        val action = intent?.action
        if (NfcAdapter.ACTION_TAG_DISCOVERED == action || NfcAdapter.ACTION_TECH_DISCOVERED == action || NfcAdapter.ACTION_NDEF_DISCOVERED == action) {
            intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES)?.also { rawMsg ->
                val msg: List<NdefMessage> = rawMsg.map { it as NdefMessage }
                if (msg.isEmpty()) return
                var textNFC = ""
                val payload = msg[0].records[0].payload
                val textEncoding = if (payload[0].toInt() and 128 == 0) {
                    charset("UTF-8")
                } else {
                    charset("UTF-16")
                }
                val languageCodeLength = (payload[0] and 51)
                try {
                    textNFC = String(payload, languageCodeLength + 1, payload.size - languageCodeLength - 1, textEncoding)
                } catch (e: UnsupportedEncodingException) {
                    Log.e(TAG, e.toString())
                }
                val paymentCard = PaymentCard(this, Authentication.getLinks())
                PaymentCard(this, Authentication.getLinks()).cardNumber = textNFC
                paymentCard.cardNumber = textNFC
                paymentCard.securityCode = "231"
                paymentCard.validityDate = "2020-12-12"
                paymentCard.pay(
                    {
                        Log.e("response", it.toString())
                        if (it.content.equals("ACCEPTED")) {
                            tvInfoPayment.text = "Payment accepted"
                            val splashTimeOut: Long = 2500
                            Cart(this, Authentication.getLinks()).removeAllProduct()
                            Handler().postDelayed({
                                startActivity(Intent(this, ProductsActivity::class.java))
                                finish()
                            }, splashTimeOut)
                        } else {
                            tvInfoPayment.text = "Card invalid"
                        }
                    },
                    {
                        tvInfoPayment.text = "Card invalid"
                        Log.e("Console", it.toString())
                    }
                )
            }
        }
    }
}