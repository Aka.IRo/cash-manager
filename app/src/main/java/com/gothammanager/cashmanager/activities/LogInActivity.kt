package com.gothammanager.cashmanager.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.gothammanager.cashmanager.R
import com.gothammanager.cashmanager.models.Authentication
import com.gothammanager.cashmanager.listeners.KeyboardEventListener
import com.gothammanager.cashmanager.showToast
import kotlinx.android.synthetic.main.activity_login.*

class LogInActivity : AppCompatActivity() {
    /**
     * Create the login view and set the listeners for the buttons
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        btn_reset.setOnClickListener {
            server_address_text.setText("")
            server_password_text.setText("")
        }

        btn_submit.setOnClickListener {
            val host = server_address_text.text
            val password = server_password_text.text
            Authentication.login(this, host.toString(), password.toString(),
                {
                    startActivity(Intent(this, MainActivity::class.java))
                    finish()
                },
                {
                    this.showToast("Wrong password")
                    Log.e(null, it.toString())
                })
        }
    }
    override fun onResume() {
        super.onResume()
        KeyboardEventListener(this) {
            Log.e("keyboardchecker", "Keyboardis open = $it")
            val icon = splash_cash_manager_image_view
            if(it) {
                icon.visibility = View.GONE
            } else {
                icon.visibility = View.VISIBLE
            }
        }
    }
}