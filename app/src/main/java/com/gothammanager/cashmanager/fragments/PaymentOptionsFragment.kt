package com.gothammanager.cashmanager.fragments

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import com.google.zxing.integration.android.IntentIntegrator
import com.gothammanager.cashmanager.R
import com.gothammanager.cashmanager.models.Authentication
import com.gothammanager.cashmanager.models.Cart
import com.gothammanager.cashmanager.models.PaymentCheck
import com.gothammanager.cashmanager.Constant.TOTAL_VALUE_KEY
import com.gothammanager.cashmanager.activities.NFCPaymentActivity
import com.gothammanager.cashmanager.activities.ProductsActivity
import com.gothammanager.cashmanager.showToast

class PaymentOptionsFragment : DialogFragment() {
    private var totalDouble: Double = 0.0
    companion object {
        var TAG: String = PaymentOptionsFragment::class.java.simpleName

        fun newInstance(totalValue: String, totalDouble: Double): PaymentOptionsFragment {
            val fragment = PaymentOptionsFragment()
            val args = Bundle()
            args.putString(TOTAL_VALUE_KEY, totalValue)
            args.putDouble("TOT_VALUE", totalDouble)
            fragment.arguments = args
            return fragment
        }
    }
    /**
     * Create the payment view and set the listeners for the buttons
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view: View = inflater.inflate(R.layout.fragment_pick_payment, container, false)
        val txvTotal = view.findViewById<TextView>(R.id.fragment_total_text_view)
        val btnCheck = view.findViewById<Button>(R.id.btn_by_check)
        val btnBluecard = view.findViewById<Button>(R.id.btn_by_bluecard)
        arguments?.let {
            txvTotal?.text = it.getString(TOTAL_VALUE_KEY)
            totalDouble = it.getDouble("TOT_VALUE")
        }

        btnCheck.setOnClickListener {
            Log.d(TAG, "listener: Closing dialog")
            val scanner = IntentIntegrator.forSupportFragment(this)
            scanner.initiateScan()
        }

        btnBluecard.setOnClickListener {
            val builder = AlertDialog.Builder(this.context)
            builder.setView(R.layout.fragment_loading)
            builder.create()
            val alertDialog: AlertDialog = builder.show()
            Handler().postDelayed({
                alertDialog.dismiss()
                val intent = Intent(this.context, NFCPaymentActivity::class.java)
                intent.putExtra(TOTAL_VALUE_KEY, txvTotal.text)
                startActivity(intent)
            }, 2000)
        }
        return view
    }

    /**
     * Takes the result of the QRcode reader & call the api
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (result != null) {
            if (result.contents == null) {
                this.context?.showToast("Could not read the QRcode", Toast.LENGTH_LONG)
            } else {
                this.context?.showToast("Scanned: " + result.contents, Toast.LENGTH_LONG)
                val builder = AlertDialog.Builder(this.context)
                builder.setView(R.layout.fragment_loading)
                builder.create()
                val alertDialog: AlertDialog = builder.show()
                val paymentCheck = PaymentCheck(this.context, Authentication.getLinks())
                paymentCheck.amount = totalDouble
                paymentCheck.order = "CashManager"
                paymentCheck.checkNumber = result.contents
                Handler().postDelayed({
                    alertDialog.dismiss()
                    paymentCheck.pay(
                        {
                            if(it.content.equals("ACCEPTED")) {
                                this.context?.showToast("Payment accepted")
                                Cart(this.context, Authentication.getLinks()).removeAllProduct(
                                    {
                                        Handler().postDelayed({
                                            startActivity(Intent(this.context, ProductsActivity::class.java))
                                        }, 1000)
                                    },
                                    {
                                        Log.e("paymentOptionsFragment", it.networkResponse.toString())
                                    }
                                )

                            } else {
                                this.context?.showToast("Wrong check number")
                            }
                        },
                        {
                            Log.e("paymentOptionFgt-pay", it.networkResponse.toString())
                        }
                    )
                }, 2000)
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }
}