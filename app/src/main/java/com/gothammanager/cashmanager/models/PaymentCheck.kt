package com.gothammanager.cashmanager.models

import android.content.Context
import com.android.volley.Request
import com.android.volley.Response
import com.gothammanager.cashmanager.network.APIRequest
import com.gothammanager.cashmanager.network.HTTPClient
import org.json.JSONObject

/**
 * Contain all bank check information and make API call to pay by bank check
 * @param context An android Context used to get HTTPClient instance
 * @param links Links Map containing all API links
 */
class PaymentCheck(context: Context?, links: Links): APayment(context, links) {
    private val PAY_CHECK = "Pay the cart with a Cheque"

    var amount: Double = 0.0
    var order: String = ""
    var checkNumber: String = ""

    /**
     * Make an API call to pay by bank check
     * @param resolve Callback called when the API call succeed
     * @param reject Callback called when the API call failed
     */
    override fun pay(resolve: Resolve<PaymentResponse>?, reject: Reject?) {
        var jsonObject = JSONObject()
        jsonObject.put("amount", amount)
        jsonObject.put("order", order)
        jsonObject.put("chequeNumber", checkNumber)
        val request = APIRequest(
            ::PaymentResponse,
            Request.Method.POST,
            links.getLink(PAY_CHECK),
            Authentication.getHTTPHeader(),
            jsonObject,
            Response.Listener {
                resolve?.invoke(it)
            },
            Response.ErrorListener {
                reject?.invoke(it)
            }
        )
        HTTPClient.getInstance(context).addToRequestQueue(request)
    }
}