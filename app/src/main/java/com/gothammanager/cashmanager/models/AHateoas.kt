package com.gothammanager.cashmanager.models

import org.json.JSONObject

/**
 * Parse a JSONObject to store all links in a Links object
 * @param jsonObject JSONObject containing all links in "_links" property
 */
abstract class AHateoas(jsonObject: JSONObject) {
    val links : Links

    init {
        if (!jsonObject.has("_links"))
            throw Error("JSONObject doesn't contain '_links' property")
        links = Links(jsonObject.getJSONObject("_links"))
    }
}