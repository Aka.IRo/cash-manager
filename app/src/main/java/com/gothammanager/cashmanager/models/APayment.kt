package com.gothammanager.cashmanager.models

import android.content.Context

/**
 *
 */
abstract class APayment(context: Context?, links: Links): AContextAPI(context, links) {
    open fun pay(resolve: Resolve<PaymentResponse>?, reject: Reject?) {
        throw Error("pay() method isn't implemented in APayment")
    }
}