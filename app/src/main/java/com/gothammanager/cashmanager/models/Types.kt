package com.gothammanager.cashmanager.models

import com.android.volley.VolleyError
import org.json.JSONObject

/**
 * Callback type called on success
 */
typealias Resolve<T> = (T) -> Unit

/**
 * Callback type called on fail
 */
typealias Reject = (VolleyError) -> Unit

/**
 * Factory type asking for a JSONObject as constructor parameter
 */
typealias Factory<T> = (JSONObject) -> T