package com.gothammanager.cashmanager.models

import android.content.Context

/**
 * All classes making API call need to have a context and a list of links
 * Inheritance from AContextAPI is an easy and reusable solution to implement them
 * @property context An Android context used to get HTTPClient instance
 * @property links Map containing all API links
 * @constructor Store a context and a map of links
 */
abstract class AContextAPI(protected val context: Context?, protected val links: Links)
