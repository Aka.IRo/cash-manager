package com.gothammanager.cashmanager.models

import org.json.JSONObject

/**
 * Used to parse Authenticate.login()'s API Call response
 * @param jsonObject JSONObject containing the token and links
 */
class LoginResponse(jsonObject: JSONObject): AHateoas(jsonObject) {
    /**
     * The user's token
     */
    val token: String

    init {
        if (!jsonObject.has("content"))
            throw Error("JSONObject doesn't contain 'content' property")
        val jsonContent = jsonObject.getJSONObject("content")
        if (!jsonContent.has("terminal"))
            throw Error("Content JSONObject doesn't contain 'terminal' property")
        token = jsonContent.getString("terminal")
    }
}