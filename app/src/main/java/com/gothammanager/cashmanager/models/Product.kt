package com.gothammanager.cashmanager.models

import org.json.JSONObject

/**
 * Parse a JSONObject and store all information about a product
 */
class Product(jsonObject: JSONObject) {

    init {
        if (!jsonObject.has("id"))
            throw Error("JSONObject doesn't contain 'id' property")
        else if (!jsonObject.has("name"))
            throw Error("JSONObject doesn't contain 'name' property")
        else if (!jsonObject.has("price"))
            throw Error("JSONObject doesn't contain 'price' property")
        else if (!jsonObject.has("quantity"))
            throw Error("JSONObject doesn't contain 'quantity' property")
        else if (!jsonObject.has("reference"))
            throw Error("JSONObject doesn't contain 'reference' property")
    }

    val id: Int = jsonObject.getInt("id")
    val name: String = jsonObject.getString("name")
    val price: Double = jsonObject.getDouble("price")
    val quantity: Int = jsonObject.getInt("quantity")
    val reference: String = jsonObject.getString("reference")

    override fun toString(): String {
        return "{id: ${id}, name: ${name}, price: ${price}, quantity: ${quantity}, reference: ${reference}}"
    }
}
object Supplier {
    var products = mutableListOf<Product>(
    )
}