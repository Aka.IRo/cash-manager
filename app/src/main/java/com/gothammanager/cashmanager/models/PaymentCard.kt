package com.gothammanager.cashmanager.models

import android.content.Context
import android.util.Log
import com.android.volley.Request
import com.android.volley.Response
import com.gothammanager.cashmanager.network.APIRequest
import com.gothammanager.cashmanager.network.HTTPClient
import org.json.JSONObject

/**
 * Contain all credit card information and make API call to pay by card
 * @param context An android Context used to get HTTPClient instance
 * @param links Links Map containing all API links
 */
class PaymentCard(context: Context, links: Links): APayment(context, links) {
    private val PAY_CARD = "Pay the cart with a Credit card"

    var status: String? = null
    var cardNumber: String = ""
    var validityDate: String = ""
    var securityCode: String = ""

    /**
     * Make an API call to pay by card
     * @param resolve Callback called when the API call succeed
     * @param reject Callback called when the API call failed
     */
    override fun pay(resolve: Resolve<PaymentResponse>?, reject: Reject?) {
        var jsonObject = JSONObject()
        jsonObject.put("cardNumber", cardNumber)
        jsonObject.put("validityDate", validityDate)
        jsonObject.put("securityCode", securityCode)
        Log.e("jsonobject",jsonObject.toString())
        val request = APIRequest(
            ::PaymentResponse,
            Request.Method.POST,
            links.getLink(PAY_CARD),
            Authentication.getHTTPHeader(),
            jsonObject,
            Response.Listener {
                resolve?.invoke(it)
            },
            Response.ErrorListener {
                reject?.invoke(it)
            }
        )
        HTTPClient.getInstance(context).addToRequestQueue(request)
    }
}