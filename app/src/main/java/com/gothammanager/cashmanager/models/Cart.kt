package com.gothammanager.cashmanager.models

import android.content.Context
import com.android.volley.Request
import com.android.volley.Response
import com.gothammanager.cashmanager.network.APIRequest
import com.gothammanager.cashmanager.network.HTTPClient
import org.json.JSONObject

/**
 * Class containing all products and API calls to manage them
 * @param context An android Context used to get HTTPClient instance
 * @param links Map containing all API links
 */
class Cart(context: Context?, links: Links): AContextAPI(context, links) {
    private val ADD_PRODUCT: String = "Add a product to the cart"
    private val REMOVE_PRODUCT: String = "Remove a product from the cart"
    private val CLEAN_CART: String = "Remove all products from the cart"

    /**
     * The price of all products
     */
    var total: Double = 0.0
    /**
     * The list of all products
     */
    var articles: MutableList<Product> = mutableListOf()

    /**
     * API call to add a new product to the Cart
     * @param reference The reference of the product
     * @param resolve Callback called when the API call succeed
     * @param reject Callback called when the API call failed
     */
    fun addProduct(reference: String, resolve: Resolve<CartResponse>? = null, reject: Reject? = null) {
        val jsonObject = JSONObject()
        jsonObject.put("reference", reference)
        val request = APIRequest(
            ::CartResponse,
            Request.Method.POST,
            links.getLink(ADD_PRODUCT),
            Authentication.getHTTPHeader(),
            jsonObject,
            Response.Listener { response ->
                total = response.total
                articles = response.articles
                resolve?.invoke(response)
            },
            Response.ErrorListener {
                reject?.invoke(it)
            }
        )
        HTTPClient.getInstance(context).addToRequestQueue(request)
    }

    /**
     * API call to remove a product from the Cart
     * @param reference The reference of the product
     * @param resolve Callback called when the API call succeed
     * @param reject Callback called when the API call failed
     */
    fun removeProduct(reference: String, resolve: Resolve<CartResponse>? = null, reject: Reject? = null) {
        val jsonObject = JSONObject()
        jsonObject.put("reference", reference)
        val request = APIRequest(
            ::CartResponse,
            Request.Method.POST,
            links.getLink(REMOVE_PRODUCT),
            Authentication.getHTTPHeader(),
            jsonObject,
            Response.Listener { response ->
                total = response.total
                articles = response.articles
                resolve?.invoke(response)
            },
            Response.ErrorListener {
                reject?.invoke(it)
            }
        )
        HTTPClient.getInstance(context).addToRequestQueue(request)
    }

    /**
     * API call to remove all products from the Cart
     * @param resolve Callback called when the API call succeed
     * @param reject Callback called when the API call failed
     */
    fun removeAllProduct(resolve: Resolve<CartResponse>? = null, reject: Reject? = null) {
        val request = APIRequest(
            ::CartResponse,
            Request.Method.POST,
            links.getLink(CLEAN_CART),
            Authentication.getHTTPHeader(),
            null,
            Response.Listener { response ->
                total = response.total
                articles = response.articles
                resolve?.invoke(response)
            },
            Response.ErrorListener {
                reject?.invoke(it)
            }
        )
        HTTPClient.getInstance(context).addToRequestQueue(request)
    }

    override fun toString(): String {
        return "{Cart: ${articles}}"
    }
}