package com.gothammanager.cashmanager.models

import org.json.JSONObject

/**
 * Used to parse Cart's API Call response
 * @param jsonObject JSONObject containing all Cart's data
 */
class CartResponse(jsonObject: JSONObject): AHateoas(jsonObject) {
    /**
     * The price of all products
     */
    val total: Double
    /**
     * The list of products
     */
    val articles: MutableList<Product>

    init {
        if (!jsonObject.has("total"))
            throw Error("JSONObject doesn't contain 'total' property")
        total = jsonObject.getDouble("total")
    }

    init {
        if (!jsonObject.has("articles"))
            throw Error("JSONObject doesn't contain 'articles' property")
        val jsonArticles = jsonObject.getJSONObject("articles")
        val iterator = jsonArticles.keys()
        var list: MutableList<Product> = mutableListOf()
        while (iterator.hasNext()) {
            val property = iterator.next()
            var jsonProduct = JSONObject(property)
            jsonProduct.put("quantity", jsonArticles.getInt(property))
            list.add(Product(jsonProduct))
        }
        articles = list
    }
}