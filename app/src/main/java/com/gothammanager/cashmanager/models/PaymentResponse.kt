package com.gothammanager.cashmanager.models

import android.util.Log
import org.json.JSONObject

/**
 * Used to parse APayment's API Call response
 * @param jsonObject JSONObject containing the token and links
 */
class PaymentResponse(jsonObject: JSONObject): AHateoas(jsonObject) {
    init {
        if (!jsonObject.has("content"))
            throw Error("JSONObject doesn't contain 'content' property")
        Log.e(null, jsonObject.toString())
    }

    /**
     * Content of the API call response
     */
    var content: String = jsonObject.getString("content")
}