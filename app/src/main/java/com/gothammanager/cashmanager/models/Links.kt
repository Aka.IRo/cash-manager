package com.gothammanager.cashmanager.models

import org.json.JSONObject

/**
 * Parse a JSONObject containing API links and store the in a map
 * @param jsonObject JSONObject containing multiple JSON object with a "href" property
 */
class Links(jsonObject: JSONObject) {
    private var links: MutableMap<String, String> = mutableMapOf()

    init {
        val iterator = jsonObject.keys()
        while (iterator.hasNext()) {
            val property = iterator.next()
            val jsonLink = jsonObject.getJSONObject(property)
            if (!jsonLink.has("href"))
                throw Error("Link JSONObject doesn't contain 'href' property")
            links[property] = jsonLink.getString("href")
        }
    }

    /**
     * Get the link having the given name
     * @param name The name of the link
     * @return Return an API link
     */
    fun getLink(name: String): String {
        return links[name]!!
    }

    override fun toString(): String {
        return links.toString()
    }
}