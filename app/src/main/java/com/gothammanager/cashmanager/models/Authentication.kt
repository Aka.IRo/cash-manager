package com.gothammanager.cashmanager.models

import android.content.Context
import android.util.Log
import com.android.volley.Request
import com.android.volley.Response
import com.gothammanager.cashmanager.network.APIRequest
import com.gothammanager.cashmanager.network.HTTPClient
import org.json.JSONObject

/**
 * Object containing authentication information
 */
object Authentication {
    private var token: String? = null
    private var links: Links? = null

    /**
     * Get the user's token
     * @exception Error Throw an error if the token is null (The user isn't authenticated)
     * @return Return the token
     */
    fun getToken(): String {
        if (token == null)
            throw Error("Token is still null. Please call login() before to use getToken().")
        return token!!
    }

    /**
     * Get the list of links obtained from login response
     * @exception Error Throw an error if links is null (The user isn't authenticated)
     * @return Return a Links object
     */
    fun getLinks(): Links {
        if (links == null)
            throw Error("Links is still null. Please call login() before to use getLinks()")
        return links!!
    }

    /**
     * API call to authenticate the user
     * @param context An android context used to get HTTPClient instance
     * @param host The IP Address or Hostname where the server is hosted (ex: 127.0.0.1:8080)
     * @param password The server password
     * @param resolve Callback called when the API call succeed
     * @param reject Callback called when the API call failed
     */
    fun login(context: Context, host: String, password: String, resolve: Resolve<LoginResponse>? = null, reject: Reject? = null) {
        val url = "http://${host}/authentication"
        Log.e("url", url)
        val jsonObject = JSONObject()
        jsonObject.put("password", password)
        val request = APIRequest(
            ::LoginResponse,
            Request.Method.POST,
            url,
            null,
            jsonObject,
            Response.Listener { response ->
                token = response.token
                links = response.links
                Log.e("Links", links.toString())
                resolve?.invoke(response)
            },
            Response.ErrorListener {
                reject?.invoke(it)
            }
        )
        HTTPClient.getInstance(context).addToRequestQueue(request)
    }

    /**
     * Get a ready to use HTTP header containing the token and specify JSON content-type
     * @return Return a map representing a HTTP header
     */
    fun getHTTPHeader(): MutableMap<String, String> {
        if (token == null)
            throw Error("Token is still null. Please call login() before to use getHTTPHeader().")
        val header: MutableMap<String, String> = mutableMapOf()
        header["Content-Type"] = "application/json"
        header["Authorization"] = token!!
        return header
    }
}