package com.gothammanager.cashmanager.network

import android.content.Context
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley

/**
 * Singleton class to expose a single HTTPClient in the whole application
 * @param context An android context to attach Volley request queue on
 */
class HTTPClient(context: Context?) {
    companion object {
        private var INSTANCE: HTTPClient? = null

        /**
         * Static function to get HTTPClient instance everywhere
         * @param context An android context to attach Volley request queue on
         */
        fun getInstance(context: Context?) =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: HTTPClient(context).also {
                    INSTANCE = it
                }
            }
    }

    /**
     * Volley request queue
     */
    val requestQueue : RequestQueue by lazy {
        Volley.newRequestQueue(context!!.applicationContext)
    }

    /**
     * Add a new request to the queue
     * @param req HTTP request
     */
    fun<T> addToRequestQueue(req: Request<T>) {
        requestQueue.add(req)
    }
}