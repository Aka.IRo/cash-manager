package com.gothammanager.cashmanager.network

import com.android.volley.NetworkResponse
import com.android.volley.ParseError
import com.android.volley.Response
import com.android.volley.Response.*
import com.android.volley.toolbox.HttpHeaderParser
import com.android.volley.toolbox.JsonRequest
import com.gothammanager.cashmanager.models.Factory
import org.json.JSONException
import org.json.JSONObject
import java.io.UnsupportedEncodingException
import java.nio.charset.Charset

/**
 * Help to create request to make API call
 * @param factory Factory function used to create the return value
 * @param method HTTP method used (GET, POST, ...)
 * @param url URL where to make the HTTP request
 * @param headers Custom headers if needed
 * @param jsonRequest HTTP request body
 * @param listener Callback called when Volley request succeed
 * @param errorListener Callback called when Volley request failed
 */
class APIRequest<T> (
    private val factory: Factory<T>,
    method: Int,
    url: String,
    private val headers: MutableMap<String, String>?,
    jsonRequest: JSONObject?,
    listener: Listener<T>,
    errorListener: ErrorListener
) : JsonRequest<T>(
    method,
    url,
    jsonRequest.toString(),
    listener,
    errorListener
) {
    /**
     * Override default request headers
     */
    override fun getHeaders(): MutableMap<String, String> = headers ?: super.getHeaders()

    /**
     * Parse HTTP request response
     */
    override fun parseNetworkResponse(response: NetworkResponse?): Response<T> {
        return try {
            val jsonString = String(
                response?.data ?: ByteArray(0),
                Charset.forName(HttpHeaderParser.parseCharset(response?.headers))
            )
            val jsonObject = JSONObject(jsonString)
            success(
                factory(jsonObject),
                HttpHeaderParser.parseCacheHeaders(response)
            )
        }
        catch (e: UnsupportedEncodingException) {
            error(ParseError(e))
        }
        catch (e: JSONException) {
            error(ParseError(e))
        }
    }
}