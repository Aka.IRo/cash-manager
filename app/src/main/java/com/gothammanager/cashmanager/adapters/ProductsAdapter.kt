package com.gothammanager.cashmanager.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.gothammanager.cashmanager.R
import com.gothammanager.cashmanager.models.Authentication
import com.gothammanager.cashmanager.models.Cart
import com.gothammanager.cashmanager.models.Product
import com.gothammanager.cashmanager.activities.TotalValueInActivity
import com.gothammanager.cashmanager.showToast
import kotlinx.android.synthetic.main.list_item.view.*

class ProductsAdapter (val context: Context, private val products: MutableList<Product>, private val listener: TotalValueInActivity) :
        RecyclerView.Adapter<ProductsAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.list_item, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return products.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val product = products[position]
        holder.setData(product, position)
    }

    /**
     * Clear the list of products
     */
    fun clearList() {
        Cart(this.context, Authentication.getLinks()).removeAllProduct(
            {
                products.clear()
                it.articles.forEach{ at -> products.add(at)}
                listener.setTotalText(it.total)
                notifyDataSetChanged()
            },
            {
                Log.e("error", it.toString())
            }
        )
    }

    /**
     * Clear the list of products
     */
    fun clearListFront() {
        products.clear()
    }

    /**
     * Call the API to add a product in the list
     * @param refProduct the reference of the product scanned
     */
    fun addProduct(refProduct: String) {
        Cart(this.context, Authentication.getLinks()).addProduct(refProduct,
            {
                products.clear()
                it.articles.forEach{ at -> products.add(at)}
                listener.setTotalText(it.total)
                notifyDataSetChanged()
            },
            {
                this.context.showToast("Article not found")
                Log.e("error", it.toString())
            }
        )
    }

    /**
     * Remove an item / one quantity from the list
     * @param int Take the position in the list
     */
    fun removeAt(pos: Int) {
        Cart(this.context, Authentication.getLinks()).removeProduct(products[pos].reference,
            {
                products.clear()
                it.articles.forEach{ at -> products.add(at)}
                listener.setTotalText(it.total)
                notifyDataSetChanged()
            },
            {
                Log.e("error", it.toString())
            }
        )
    }

    /**
     * Class that will generate and handle the list
     */
    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var currentProduct: Product? = null
        var currentPosition: Int = 0

        init {
            itemView.setOnClickListener {
                currentProduct?.let {
                    context.showToast(currentProduct!!.name + "click", Toast.LENGTH_LONG)
                }
            }
            itemView.imgShare.setOnClickListener {
                currentProduct?.let {
                    removeAt(adapterPosition)
                }
            }
        }

        fun setData(product: Product?, pos: Int){
            product?.let {
                itemView.product_name_text_view.text = product.name
                itemView.product_price_text_view.text = product.price.toString()
                itemView.product_quantity_text_view.text = product.quantity.toString()
                this.currentProduct = product
                this.currentPosition = pos
            }
        }
    }
}